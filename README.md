LeetCode Challenges Repository

This repository contains a collection of solutions to various LeetCode coding challenges that I have solved. Each solution is organized by its corresponding LeetCode problem Name In a propper subject folder.

Table of Contents

Introduction
Directory Structure
How to Use
Contributing
License
Introduction

LeetCode is a platform that provides a wide range of coding challenges to improve algorithmic and problem-solving skills. This repository serves as a personal record of my solutions to these challenges. Feel free to explore and learn from the code!

Directory Structure

The repository is organized in the following way:
/
|-- main/
|   |-- Developer/
|       |-- solutions/
|           |-- Arrays_And_Hashmaps/
|               |-- Problem-001/
|                   |-- Solution.py
|               |-- Problem-002/
|                   |-- Solution.py
|           |-- Stack/
|               |-- Problem-003/
|                   |-- Solution.java
|               |-- Problem-004/
|                   |-- Solution.py
|           |-- Linked_List/
|               |-- Problem-005/
|                   |-- Solution.cpp
|               |-- Problem-006/
|                   |-- Solution.py
|           |-- ...

Each problem has its own directory named "X," where X is the LeetCode problem name. Inside each directory, you will find the solution file (X.py).

How to Use

Explore Solutions: Navigate to the specific problem directory to find the solution file.
Contribute: If you have a different or more optimized solution, feel free to contribute! Follow the guidelines in the Contributing section.
Learn and Practice: Use this repository as a resource to learn and practice coding challenges. Analyze the solutions and try solving the problems on your own before referring to the code.
Contributing

If you would like to contribute your solutions:

Fork the repository.
Create a new branch for your changes: git checkout -b feature/new-solution.
Add your solution to the respective problem directory.
Update the README file with relevant information.
Commit your changes: git commit -m "Add solution for Problem-X".
Push to the branch: git push origin feature/new-solution.
Create a pull request.
