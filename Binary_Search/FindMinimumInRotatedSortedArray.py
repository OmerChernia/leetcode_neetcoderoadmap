class Solution(object):

    def findMin(self,nums):
        l , r = 0, len(nums) -1

        if r == 0:
            return nums[r]

        while(l<=r):
            m = (r+l)//2
            if l == r+1:
                return min(nums[l] and nums[r])
            elif nums[m] < nums[m+1] and nums[m] < nums[m-1]: #nums[m] is the minimum
                return nums[m]
            elif nums[m] > nums[m+1] and nums[m] > nums[m-1]:#nums[m] is the maximum
                return nums[m+1]
            elif nums[m] > nums[l] and nums[m] > nums[r]:
                l = m+1
            else:
                r = m-1

        return -1
