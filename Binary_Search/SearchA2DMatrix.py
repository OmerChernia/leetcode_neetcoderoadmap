class Solution(object):
    def searchMatrix(self,matrix, target):

        one_d_array = []
        for sublist in matrix:
            for element in sublist:
                one_d_array.append(element)

        l, r = 0, len(one_d_array) - 1

        while (l <= r):
            m = int((r + l) / 2)
            if one_d_array[m] < target:
                l = m + 1
            elif one_d_array[m] > target:
                r = m - 1
            elif one_d_array[m] == target:
                return True

        return False