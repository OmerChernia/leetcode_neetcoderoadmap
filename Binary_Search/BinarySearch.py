class Solution(object):
    def search(self, nums, target):
        r, l = 0, len(nums) -1

        while (l <= r):
            m = int(r + l / 2)
            if nums[m] < target:
                l = m - 1
            elif nums[m] > target:
                r = m+1
            elif nums[m] == target:
                return m

        return -1