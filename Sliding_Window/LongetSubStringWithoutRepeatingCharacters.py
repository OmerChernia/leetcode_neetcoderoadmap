class Solution(object):
    def lengthOfLongestSubstring(self,s):
        temp = []
        count = 0
        max_count = 0

        for c in s:
            if c in temp:
                temp = temp[temp.index(c) + 1:]

            temp.append(c)
            count = len(temp)
            max_count = max(max_count, count)

        return max_count