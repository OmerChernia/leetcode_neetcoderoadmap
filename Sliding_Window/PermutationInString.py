class Solution(object):
    def checkInclusion(self,s1, s2):
        window_size = len(s1)
        s1_counts = [0] * 26  # Assuming only lowercase English letters

        # Count the occurrences of characters in s1
        for char in s1:
            s1_counts[ord(char) - ord('a')] += 1

        # Initialize the counts for the first window in s2
        window_counts = [0] * 26
        for i in range(window_size):
            window_counts[ord(s2[i]) - ord('a')] += 1

        for i in range(len(s2) - window_size + 1):
            # Check if the current window is a permutation of s1
            if window_counts == s1_counts:
                return True

            # Move the window to the right
            if i + window_size < len(s2):
                window_counts[ord(s2[i]) - ord('a')] -= 1
                window_counts[ord(s2[i + window_size]) - ord('a')] += 1

        return False