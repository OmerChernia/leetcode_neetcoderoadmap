class Solution(object):
    def evalRPN(self,tokens):
        a = 0
        b = 0
        ops = {'+': lambda x, y: x + y,
               '-': lambda x, y: x - y,
               '*': lambda x, y: x * y,
               '/': lambda x, y: int(x / y)}
        sul_stck = []

        for i in tokens:
            if i not in ops:
                sul_stck.append(int(i))
            elif len(sul_stck) >=2:
                b = sul_stck.pop()
                a = sul_stck.pop()
                s = ops[i](a,b)
                sul_stck.append(s)

        return sul_stck[-1]