class MinStack(object):
    def __init__(self):
        self.stck = []
        self.min_stck = []

    def push(self, val):
        self.stck.append(val)
        val = min(val, self.min_stck[-1] if self.min_stck else val)
        self.min_stck.append(val)

    def pop(self):
        self.stck.pop()
        self.min_stck.pop()

    def top(self):
        return self.stck[-1]

    def getMin(self):
        return self.min_stck[-1]