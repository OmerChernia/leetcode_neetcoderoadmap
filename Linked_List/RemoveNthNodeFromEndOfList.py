class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution(object):
    def removeNthFromEnd(self,head, n):
        dummy = ListNode(0)
        dummy.next = head
        slow = fast = dummy

        # Move fast pointer n+1 steps ahead
        for _ in range(n + 1):
            if fast is not None:
                fast = fast.next

        # Move both slow and fast pointers until fast reaches the end
        while fast is not None:
            slow = slow.next
            fast = fast.next

        # Remove the nth node from the end
        if slow.next is not None:
            slow.next = slow.next.next

        return dummy.next