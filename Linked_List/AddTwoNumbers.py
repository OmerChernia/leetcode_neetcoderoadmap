class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution(object):
    def addTwoNumbers(self,l1, l2):
        counter1 = 0
        counter2 = 0
        res = 0
        sum1 = 0
        sum2 = 0
        ret = ListNode(0)

        while l1 is not None:
            sum1 += l1.val * (10^counter1)
            counter1 += 1
            l1 = l1.next

        while l2 is not None:
            sum2 += l2.val * (10^counter2)
            counter2 += 1
            l2 = l2.next

        res = sum1+sum2
        curr = ret.next

        for c in str(res):
            curr = ListNode(int(c))
            curr = curr.next

        return ret.next