class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution(object):
    def reorderList(head):
        def reverseList(l):
            prev = None
            curr = l

            while curr is not None:
                new_node = ListNode(curr.val)
                new_node.next = prev
                prev = new_node
                curr = curr.next

            return prev

        p = head
        len = 0

        while p != None:
            len += 1
            p = p.next

        flip_list = reverseList(head)

        pl = head.next
        pr = flip_list

        pl_next = pl.next
        pr_next = pr.next

        curr = head

        for i in range(len // 2):
            curr.next = pr
            curr = curr.next
            curr.next = pl
            pl = pl.next
            pr = pr.next

            if pl.next is None or pr.next is None:
                break


        return head