class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def hasCycle(self, head):
        dummy = ListNode(0)
        p1 = dummy
        p2 = dummy
        dummy = dummy.next
        p1_ctr = 0
        p2_ctr = 0
        flag = 0

        while p2.next is not None:
            p1 = p1.next
            p2 = p2.next.next

            if p2.next == p1:
                return 1

            elif p1_ctr != 0 and p1 == p2:
                return 1

        return 0