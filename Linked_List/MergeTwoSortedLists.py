class ListNode(object):
   def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution(object):
    def mergeTwoLists(self,list1, list2):
        p1 = list1
        p2 = list2

        ret_head = ListNode()
        current = ret_head

        while p1 is not None and p2 is not None:
            if p1.val < p2.val:
                current.next = p1
                p1 = p1.next
            else:
                current.next = p2
                p2 = p2.next

            current = current.next

        if p1 is None:
            current.next = p2
        else:
            current.next = p1

        return ret_head.next