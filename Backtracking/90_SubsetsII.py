class Solution(object):
    def subsetsWithDup(self, nums):
        res = []
        subset = []

        def backtrck(i):
            if i >= len(nums):
                it = tuple(subset)
                it = sorted(it)
                if it not in res:
                    res.append(it)
                return

            subset.append(nums[i])
            backtrck(i + 1)

            subset.pop()
            backtrck(i + 1)

        backtrck(0)
        return res
