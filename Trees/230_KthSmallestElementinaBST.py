# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution(object):
    def kthSmallest(self, root, k):
        def make_List(root, arr = None):
            if arr is None:
                arr = []

            if not root:
                return arr

            make_List(root.left, arr)
            arr.append(root.val)
            make_List(root.right, arr)

            return arr

        ret = []

        make_List(root, ret)
        return ret[k-1]


