# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution(object):
    def levelOrder(self, root):
        if root is None:
            return []

        queue = [root]
        ret_arr = []

        while queue:
            level_size = len(queue)
            temp_arr = []

            for i in range(level_size):
                cur_node = queue.pop(0)
                temp_arr.append(cur_node.val)

                if cur_node.left:
                    queue.append(cur_node.left)

                if cur_node.right:
                    queue.append(cur_node.right)

            ret_arr.append(temp_arr)

        return ret_arr
