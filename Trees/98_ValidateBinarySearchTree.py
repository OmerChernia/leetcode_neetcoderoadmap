# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution(object):
    class Solution:
        def isValidBST(self, root):
            def BST(root, min_val=float('-inf'), max_val=float('inf')):
                if not root:
                    return True

                if min_val < root.val < max_val:
                    left = BST(root.left, min_val, root.val)
                    right = BST(root.right, root.val, max_val)

                    return left and right

                else:
                    return False

            return BST(root)


