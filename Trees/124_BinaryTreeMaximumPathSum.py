# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution(object):
    def maxPathSum(self, root):
        global global_max
        global_max = float('-inf')  # global variable

        def maxPath(node):
            global global_max

            if not node:
                return 0

            left = max(0, maxPath(node.left))  # Ignore negative values
            right = max(0, maxPath(node.right))

            global_max = max(global_max, node.val + left + right)

            return node.val + max(left, right)

        maxPath(root)

        return global_max



