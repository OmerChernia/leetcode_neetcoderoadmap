class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution(object):
    def invertTree(self, root):

        if not root:
            return None

        if not root.left and not root.right:
            return root
        else:
            root.left = self.invertTree(root.left)
            root.right = self.invertTree(root.right)
            temp = root.left
            root.left = root.right
            root.right = temp

        return root
