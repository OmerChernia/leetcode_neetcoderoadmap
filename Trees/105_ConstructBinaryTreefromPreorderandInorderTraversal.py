# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution(object):
    def buildTree(self, preorder, inorder):
        if not preorder or not inorder:
            return None

        ret = TreeNode(preorder[0])

        if not preorder or not inorder:
            return None

        index = inorder.index(ret.val)
        in_left = inorder[0:index]
        in_right = inorder[index + 1:]

        pre_left = preorder[1: 1 + index]
        pre_right = preorder[1 + index:]

        left = self.buildTree(pre_left, in_left)
        right = self.buildTree(pre_right, in_right)

        ret.left = left
        ret.right = right

        return ret