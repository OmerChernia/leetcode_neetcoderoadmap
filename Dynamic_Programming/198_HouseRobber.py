class Solution(object):
    def rob(self, nums):
        res = []

        if len(nums) == 0:
            return
        elif len(nums) == 1:
            return nums[0]
        elif len(nums) == 2:
            return max(nums[0], nums[1])

        else:
            res.append(nums[0])
            res.append(max(nums[0], nums[1]))

            for i in range(2, len(nums)):
                res.append(max(res[-1], nums[i] + res[i-2]))

            return max(res)



