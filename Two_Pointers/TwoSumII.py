class Solution(object):
    def twoSum(self, numbers, target):
        sul = []
        j = len(numbers) -1
        i = 0

        while i < j:
            sum = numbers[i] + numbers[j]
            if sum > target:
                j -= 1
            elif sum == target:
                return [i +1, j+1]
            elif sum < target:
                i+= 1

        return []
