class Solution(object):
    def threeSum(self, nums):
        sort_nums = sorted(nums)
        n = len(nums)
        sul = []

        for a in range(n - 2):
            if a > 0 and sort_nums[a] == sort_nums[a - 1]:
                continue
            b = a + 1
            c = n - 1

            while b < c:
                total_sum = sort_nums[a] + sort_nums[b] + sort_nums[c]

                if total_sum == 0:
                    sul.append([sort_nums[a], sort_nums[b], sort_nums[c]])

                    # Skip duplicate values of b and c
                    while b < c and sort_nums[b] == sort_nums[b + 1]:
                        b += 1
                    while b < c and sort_nums[c] == sort_nums[c - 1]:
                        c -= 1

                    b += 1
                    c -= 1
                elif total_sum < 0:
                    b += 1
                else:
                    c -= 1

        return sul