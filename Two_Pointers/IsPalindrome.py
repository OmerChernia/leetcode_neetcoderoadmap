class Solution(object):
    def isPalindrome(self,s):
        def clean_and_lower(input_string):
            # Remove non-alphanumeric characters and convert to lowercase
            cleaned_string = ''.join(char.lower() for char in input_string if char.isalnum())
            return cleaned_string

        fixed_s = clean_and_lower(s)
        s_len = len(fixed_s) -1
        for i, n in enumerate(fixed_s):
            if n != fixed_s[s_len]:
                return False
            if i >= s_len:
                break
            s_len -= 1
        return True
