class Solution(object):
    def maxArea(self, height):
        max = float('-inf')
        l = 0
        r = len(height)-1
        shift = len(height)-1

        while l < r:
            a = height[l]
            b = height[r]
            vol = min(a,b) * shift
            if max < vol:
                max = vol
            if a < b:
                l += 1
            else:
                r -= 1

            shift -= 1

        return max