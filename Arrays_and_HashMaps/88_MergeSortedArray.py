class Solution(object):
    def merge(self, nums1, m, nums2, n):
        i, j = 0, 0
        res = []

        if m == 0:
            return nums2

        elif n == 0:
            return nums1

        else:

            while (i < m and j < n):
                if (nums1[i] <= nums2[j]):
                    res.append(nums1[i])
                    i += 1
                else:
                    res.append(nums2[j])
                    j += 1

            res+= nums1[i:m] + nums2[j:n]

        return res
