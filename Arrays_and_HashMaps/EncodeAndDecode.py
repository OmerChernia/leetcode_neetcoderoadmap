class Solution(object):
    def encode(self, strs):
        sul = ""
        for val in strs:
            sul += ';'
            sul += val
            sul += ':'

        new_sul = sul[1:-1]

        return new_sul

    def decode(self, str):
        sul = []
        word = ""
        for i in range(len(str)):
            if str[i] != ':' and str[i] != ';':
                word += str[i]
            elif str[i] == ";" and str[i-1] != ':':
                word+= str[i]
            elif str[i-1] == ':' and str[i+1] == ';':
                word += str[i]

            if str[i] == ';' and str[i-1] == ':' or i == len(str)-1:
                sul.append(word)
                word = ""

        return sul