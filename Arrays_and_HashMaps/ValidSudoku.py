class Solution(object):
    def isValidSudoku(self,board):
        def is_valid(arr):
            bucket_checks = [0] * 10
            for num in arr:
                if num != ".":
                    num = int(num)
                    bucket_checks[num] += 1
                    if bucket_checks[num] > 1:
                        return False
            return True

        # Check rows
        for row in board:
            if not is_valid(row):
                return False

        # Check columns
        for col in zip(*board):
            if not is_valid(col):
                return False

        # Check 3x3 subgrids
        for i in range(0, 9, 3):
            for j in range(0, 9, 3):
                subgrid = [board[x][y] for x in range(i, i+3) for y in range(j, j+3)]
                if not is_valid(subgrid):
                    return False

        return True
