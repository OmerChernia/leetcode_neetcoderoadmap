from collections import defaultdict
class Solution(object):
    def groupAnagrams(self, strs): #using default dict
        ret_map = defaultdict(list)
        result = []

        for s in strs:
            sorted_s = ''.join(sorted(s))
            ret_map[sorted_s].append(s)

        for value in ret_map.values():
            result.append(value)

        return result