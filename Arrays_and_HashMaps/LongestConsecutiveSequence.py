class Solution(object):
    def longestConsecutive(self, nums):
        if not nums:
            return 0

        nums_set = set(nums)
        lon_con = 1
        current_streak = 0
        current_num = 0

        for i in nums_set:
            if i-1 not in nums_set:
                current_num = i
                current_streak = 1

            while current_num + 1 in nums_set:
                current_num += 1
                current_streak += 1

            lon_con = max(current_streak, lon_con)

        return lon_con