from collections import defaultdict

class Solution(object):
    # Solution Number 1 - bad runtime, good memory
    def topKFrequent1(self, nums, k):
        ret_map = defaultdict(int)
        ans = []

        for i in nums:
            ret_map[i] += 1

        for i in range(k):
            max_val = max(ret_map, key=ret_map.get)
            ans.append(max_val)
            del ret_map[max_val]

        return ans


    # Solution Number 2
    def topKFrequent2(self, nums, k):
        count = {}
        freq = [[] for i in range(len(nums) + 1)]

        for n in nums:
            count[n] = 1 + count.get(n, 0)

        for n, c in count.items():
            freq[c].append(n)

        res = []
        for i in range(len(freq) - 1, 0, -1):
            for n in freq[i]:
                res.append(n)
                if len(res) == k:
                    return res
        return res