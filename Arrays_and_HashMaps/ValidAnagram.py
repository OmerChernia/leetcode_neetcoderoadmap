class Solution(object):
    #Solution Number 1 - using sorting:
    def isAnagram1(self, s, t):
        if len(s) != len(t):
            return False
        else:
            sortedS = ''.join(sorted(s))
            sortedT = ''.join(sorted(t))

        if sortedS == sortedT:
            return True
        else:
            return False

    # Solution Number 2 - using Hashmaps:
    def isAnagram2(self, s, t):
        if len(s) != len(t):
            return False

        s_map = {}
        t_map = {}

        for i in range(len(s)):
            s_map[s[i]] = 1+s_map.get(s[i],0) #python way to avoid keyError
            t_map[t[i]] = 1+t_map.get(t[i],0)

        for c in s_map:
            if s_map[c] != t_map.get(c,0):
                return False

        return True