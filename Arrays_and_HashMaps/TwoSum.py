class Solution(object):
    #Solution Number 1 - O(n^2)
    def twoSum1(self, nums, target):
        ret = []
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                if nums[i] + nums[j] == target:
                    ret.append(i)
                    ret.append(j)
                    return ret

        return ret

    # Solution Number 2 - O(n), using HashMap
    def twoSum2(self, nums, target):
        ret_map = {} # index : value

        for i,n in enumerate(nums):
            diff = target - n
            if diff in ret_map:
                return [ret_map[diff] ,i]
            ret_map[n] = i

        return