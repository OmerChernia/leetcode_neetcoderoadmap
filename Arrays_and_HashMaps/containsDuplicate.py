
class Solution(object):
    def containsDuplicate(self, nums):
        seen_set = set()

        for num in nums:
            if num in seen_set:
                return True
            seen_set.add(num)

        return False